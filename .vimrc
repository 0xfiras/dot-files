call plug#begin()

Plug 'dracula/vim', { 'as': 'dracula' }

Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }

Plug 'pboettch/vim-cmake-syntax'

Plug 'Valloric/YouCompleteMe'

Plug 'vim-scripts/indentpython.vim'
Plug 'nvie/vim-flake8'

set number
set encoding=utf-8

syntax on

map <C-n> :NERDTreeToggle<CR>
